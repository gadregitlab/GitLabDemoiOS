//
//  ViewController.m
//  GitLabDemoiOS
//
//  Created by Amey Gadre on 18/02/17.
//  Copyright © 2017 CitiusTech. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Printing Data");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
